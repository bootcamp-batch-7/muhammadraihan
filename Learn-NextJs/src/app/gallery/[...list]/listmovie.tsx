"use client"
import Satellite from '@services/satellite';
import React, { useEffect, useState } from 'react';
import Gallery from '../page';
interface MovieDetailsProps {
  movieId: number;
}

const MovieDetails: React.FC<MovieDetailsProps> = ({ movieId }) => {
  const [movieDetails, setMovieDetails] = useState<any>(null);

  useEffect(() => {
    async function getMovieDetails() {
      try {
        const response = await Satellite.get(`https://api.themoviedb.org/3/movie/${movieId}`, {
          headers: {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmOTM0MGY4MGJiODk3MjRjMzAyOTE2NWYwZTU0ZGZkNCIsInN1YiI6IjY0YmVhZDdjYjg2NWViMDBlMjA3ODdmYSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.6SVF4TKvcVAADioiC_qPi4dHgdYgSE5tGV7GF8r73Bo',
          },
        });
        console.log(response.data)
        setMovieDetails(response.data);
      } catch (error) {
        console.log("ERROR DATA" + error);
      }
    }

    getMovieDetails();
  }, [movieId]);

  console.log("movie id", movieId);

  return (
    <div>
      <Gallery>
        <div>
          {movieDetails && (
            <div>
              <h1 className="justify-items-center text-5xl text-center my-5">{movieDetails.title}</h1>
              <div className='flex flex-row justify-center'>
                <div>
                  <img src={`https://image.tmdb.org/t/p/original/${movieDetails.poster_path}`} alt='Profile' className='w-64 h-auto mx-auto rounded-lg' />
                </div>
                <div className='mx-100 ml-4'>
                  <p className='text-s font-bold mt-4'>Popularity: {movieDetails.popularity}</p>
                  <p className='text-s font-bold mt-4'>Revenue: {movieDetails.revenue}</p>
                  <p className='text-s font-bold mt-4'>Runtime: {movieDetails.runtime}</p>
                  <p className='text-s font-bold mt-4'>Status: {movieDetails.status}</p>
                  <p className='text-s font-bold mt-4'>Vote Count: {movieDetails.vote_average}</p>
                  <p className='text-s font-bold mt-4'>Vote Average: {movieDetails.vote_count}</p>
                </div>
              </div>
            </div>
          )}
        </div>
      </Gallery>
    </div>
  );
};
export default MovieDetails;