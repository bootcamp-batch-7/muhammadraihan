import { View, ImageBackground, TextInput, TouchableOpacity, ActivityIndicator, ScrollView } from 'react-native'
import React, { useEffect, useState } from 'react'
import image from '../../assets/img/index'
import { HEIGHT, WIDTH, Fonts } from '../../assets/styles'
import { ArrowBack } from '../../assets/svg'
import Text from '../../components/Text'
import { Eye, EyeSlash } from '../../assets/svg'
import Satellite from '../../services/satellite'

export default function Register({ router, navigation }) {

  const [isEnable, setIsEnable] = useState(true);
  const [showPass, setShowPass] = useState(true);
  const [showConfirmPass, setShowConfirmPass] = useState(true)

  const [name, setName] = useState("");
  const [errorName, setErrorName] = useState("");

  const [email, setEmail] = useState("");
  const [errorEmail, setErrorEmail] = useState("");

  const [phone, setPhone] = useState("");
  const [errorPhone, setErrorPhone] = useState("");

  const [nik, setNik] = useState("");
  const [errorNik, setErrorNik] = useState("");

  const [password, setPassword] = useState("");
  const [errorPassword, setErrorPassword] = useState("");

  const [confirmPassword, setConfirmPassword] = useState("");
  const [errorConfirmPassword, setErrorConfirmPassword] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (name !== "" &&
      email !== "" &&
      phone !== "" &&
      nik !== "" &&
      password !== "" &&
      confirmPassword !== "" &&
      errorName === "" &&
      errorEmail === "" &&
      errorPhone === "" &&
      errorNik === "" &&
      errorPassword === "" &&
      errorConfirmPassword === "") {
      setIsEnable(false)
    } else {
      setIsEnable(true)
    }
  }, [
    name,
    email,
    phone,
    nik,
    password,
    confirmPassword,
    errorName,
    errorEmail,
    errorPhone,
    errorNik,
    errorPassword,
    errorConfirmPassword
  ]);

  const onSubmit = async () => {
    setIsLoading(true);
    const body = {
      doSendRegister: {
        name: name,
        email: email,
        phoneNumber: phone,
        password: password,
        nik: nik
      }
    };

    console.log(JSON.stringify(body, null, 2));
    Satellite.post("auth/register", body)
      .then((response) => {
        if (response.status === 200) {
          console.log("Response", response)
          navigation.navigate("Login")
        } else {
          console.log("Error", response.message)
          setErrorEmail("Email already registered")
        }
      })
      .catch((error) => {
        console.log("Error", error.message)
        setErrorEmail("Email already registered")
      })
      .finally(() => {
        setTimeout(() => {
          setIsLoading(false);
        }, 1000)

      })

  }

  return (
    <ImageBackground
      source={image.BG_LOGIN}
      style={{ width: WIDTH, height: HEIGHT }}
      resizeMode='cover'
    >
      <ScrollView>
      <View style={{ marginTop: 54 }}>
        <ArrowBack stroke={'white'} marginHorizontal={16} onPress={() => navigation.goBack()}></ArrowBack>
        <View style={{ marginTop: 54, marginHorizontal: 16 }}>
          <View>
            <Text color='#FFF' fontSize={16} bold>Name</Text>
          </View>
          <View style={{
            borderWidth: 1,
            marginTop: 8,
            borderColor: errorName ? '#EA8685' : '#132040',
            borderRadius: 8,
            padding: 12,
            backgroundColor: '#273C75'
          }}>
            <TextInput
              placeholder={'Enter Your Name'}
              placeholderTextColor={'#D3D3D3'}
              style={{
                color: '#FFF',
                fontFamily: Fonts.Nunito.Bold,
                fontSize: 16
              }}
              onChangeText={(value) => {
                setName(value);
                if (value === "") {
                  setErrorName("Name must be filled in");
                  return;
                }
                setErrorName("");
              }}
            />
          </View>
          <Text color='#EA8685' style={{ textAlign: "right", marginRight: 8 }}>{errorName}</Text>
        </View>
      </View>
      <View style={{ marginTop: 27, marginHorizontal: 16 }}>
        <View>
          <Text color='#FFF' fontSize={16} bold>Email</Text>
        </View>
        <View style={{
          borderWidth: 1,
          marginTop: 8,
          borderColor: errorEmail ? '#EA8685' : '#132040',
          borderRadius: 8,
          padding: 12,
          backgroundColor: '#273C75'
        }}>
          <TextInput
            keyboardType='email-address'
            placeholder={'Enter Your Email'}
            placeholderTextColor={'#D3D3D3'}
            style={{
              color: '#FFF',
              fontFamily: Fonts.Nunito.Bold,
              fontSize: 16
            }}
            onChangeText={(value) => {
              setEmail(value);
              const emailRegex =
                /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
              if (value === "") {
                setErrorEmail("Email must be filled in");
                return;
              }
              if (!emailRegex.test(value)) {
                setErrorEmail("Invalid Mail Address")
                return;
              }
              setErrorEmail("");
            }}
          />
        </View>
        <Text color='#EA8685' style={{ textAlign: "right", marginRight: 8 }}>{errorEmail}</Text>
      </View>

      <View style={{ marginTop: 27, marginHorizontal: 16 }}>
        <View>
          <Text color='#FFF' fontSize={16} bold>Phone</Text>
        </View>
        <View style={{
          borderWidth: 1,
          marginTop: 8,
          borderColor: errorPhone ? '#EA8685' : '#132040',
          borderRadius: 8,
          padding: 12,
          backgroundColor: '#273C75'
        }}>
          <TextInput
            keyboardType='number-pad'
            placeholder={'Enter Your Phone'}
            placeholderTextColor={'#D3D3D3'}
            style={{
              color: '#FFF',
              fontFamily: Fonts.Nunito.Bold,
              fontSize: 16
            }}
            onChangeText={(value) => {
              setPhone(value);
              const regexNumber = /^[0-9]+$/;
              if (value === "") {
                setErrorPhone("Phone must be filled in");
                return;
              }
              if (!regexNumber.test(value)) {
                setErrorPhone("Phone only contain numbers");
                return;
              }
              setErrorPhone("");
            }}
          />
        </View>
        <Text color='#EA8685' style={{ textAlign: "right", marginRight: 8 }}>{errorPhone}</Text>
      </View>
      <View style={{ marginTop: 27, marginHorizontal: 16 }}>
        <View>
          <Text color='#FFF' fontSize={16} bold>NIK</Text>
        </View>
        <View style={{
          borderWidth: 1,
          marginTop: 8,
          borderColor: errorNik ? '#EA8685' : '#132040',
          borderRadius: 8,
          padding: 12,
          backgroundColor: '#273C75'
        }}>
          <TextInput
            placeholder={'Enter Your NIK'}
            placeholderTextColor={'#D3D3D3'}
            style={{
              color: '#FFF',
              fontFamily: Fonts.Nunito.Bold,
              fontSize: 16
            }}
            onChangeText={(value) => {
              setNik(value);
              const regexNumber = /^[0-9]+$/;

              if (value === "") {
                setErrorNik("NIK must be filled in");
                return;
              }
              if (!regexNumber.test(value)) {
                setErrorNik("NIK only contain numbers")
                return;
              }
              if (value.length !== 16) {
                setErrorNik("NIK must be 16 digits");
                return;
              }
              setErrorNik("");
            }}
          />
        </View>
        <Text color='#EA8685' style={{ textAlign: "right", marginRight: 8 }}>{errorNik}</Text>
      </View>
      <View style={{ marginTop: 27, marginHorizontal: 16 }}>
        <View>
          <Text color='#FFF' fontSize={16} bold>Password</Text>
        </View>
        <View style={{
          borderWidth: 1,
          marginTop: 8,
          borderColor: errorPassword ? '#EA8685' : '#132040',
          borderRadius: 8,
          padding: 12,
          backgroundColor: '#273C75',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}>
          <TextInput
            secureTextEntry={showPass}
            placeholder={'Password'}
            placeholderTextColor={'#D3D3D3'}
            style={{
              color: '#FFF',
              fontFamily: Fonts.Nunito.Bold,
              fontSize: 16
            }}
            onChangeText={(value) => {
              setPassword(value);
              if (value === "") {
                setErrorPassword("Password must be filled in");
                return;
              }
              setErrorPassword("");
            }}
          />
          <TouchableOpacity onPress={() => setShowPass(!showPass)}>
            {showPass ? (
              <Eye width={20} height={20} stroke={'#FFF'}></Eye>
            ) : (<EyeSlash width={20} height={20} stroke={'#FFF'}></EyeSlash>)}

          </TouchableOpacity>
        </View>
        <Text color='#EA8685' style={{ textAlign: "right", marginRight: 8 }}>{errorPassword}</Text>
      </View>
      <View style={{ marginTop: 27, marginHorizontal: 16 }}>
        <View>
          <Text color='#FFF' fontSize={16} bold>Confirm Password</Text>
        </View>
        <View style={{
          borderWidth: 1,
          marginTop: 8,
          borderColor: errorConfirmPassword ? '#EA8685' : '#132040',
          borderRadius: 8,
          padding: 12,
          backgroundColor: '#273C75',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}>
          <TextInput
            secureTextEntry={showConfirmPass}
            placeholder={'Confirm Password'}
            placeholderTextColor={'#D3D3D3'}
            style={{
              color: '#FFF',
              fontFamily: Fonts.Nunito.Bold,
              fontSize: 16
            }}
            onChangeText={(value) => {
              setConfirmPassword(value);
              if (value === "") {
                setErrorConfirmPassword("Confirm your password");
                return;
              }
              if (value !== password) {
                setErrorConfirmPassword("Password doesn't match");
                return;
              }
              setErrorConfirmPassword("");
            }}
          />
          <TouchableOpacity onPress={() => setShowConfirmPass(!showConfirmPass)}>
            {showConfirmPass ? (
              <Eye width={20} height={20} stroke={'#FFF'}></Eye>
            ) : (<EyeSlash width={20} height={20} stroke={'#FFF'}></EyeSlash>)}

          </TouchableOpacity>
        </View>
        <Text color='#EA8685' style={{ textAlign: "right", marginRight: 8 }}>{errorConfirmPassword}</Text>
        <TouchableOpacity
          disabled={isEnable || isLoading}
          onPress={onSubmit}
          style={{
            backgroundColor: '#18DCFF',
            borderRadius: 8,
            paddingVertical: 12,
            alignItems: 'center',
            marginTop: 27,
            opacity: isEnable || isLoading ? 0.5 : 1
          }}>
          {isLoading ? (
            <ActivityIndicator size={'small'} color='#261A31'></ActivityIndicator>
          ) : (
            <Text color='#261A31' fontSize={16} bold>Register</Text>
          )}
          
        </TouchableOpacity>
      </View>
      </ScrollView>


    </ImageBackground>
  )
}