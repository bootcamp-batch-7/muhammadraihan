import React, {useEffect} from 'react';
import { View } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import IndexLayout from './app/index';
import * as Font from 'expo-font';
import fonts from './app/assets/fonts/index';
import * as SplashScreen from 'expo-splash-screen';


export default function App() {
  useEffect (() => {
    async function loadFonts(){
      try{
        await Font.loadAsync(fonts);
      }catch(error){
        console.warn(error);
      }finally{
        await SplashScreen.hideAsync();
      }
    }

    loadFonts();
  }, [])
  return (
    <SafeAreaProvider style={{flex: 1}}>
        <IndexLayout/>
    </SafeAreaProvider>
  )
}